<?php

namespace app\controllers;

use Yii;
use app\models\Patient;
use app\models\PatientSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * PatientController implements the CRUD actions for Patient model.
 */
class PatientController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Patient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PatientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Patient model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Patient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Patient();
        $model->scenario = 'create';
        $regNo = Patient::find()->select('reg_no')->orderBy(['id'=> SORT_DESC])->one();
        $num = explode("-",$regNo['reg_no']);
        $num[1]++;
        //echo 'Ahsan';
        /*echo "<pre>";
        echo print_r($regNo);
        echo "</pre>";*/

        $model->reg_no = 'HC-'.$num[1];//$regNo['reg_no'];
        //$model->created_by = Yii::$app->user->id;
        //$model->created_on = date("Y-m-d H:i:s");
        $model->status = '1';
        $model->created_by = Yii::$app->user->id;
        $model->created_on = date("Y-m-d H:i:s");
        $model->referred_by_id = 1;
        $model->panel_id = 1;
        $model->whatsapp_no = '0333';
        $model->country = 'Pakistan';

        //echo "Create";
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            /*echo "<pre>";
            echo print_r($model);
            echo "</pre>";*/
            if(isset($_GET['check-in']))
            {
                $response['id'] = $model->id;
                $response['name'] = $model->name;
                $response['age'] = $model->age;
                $response['phone'] = $model->phone_no;
                $response['gender'] = $model->gender;
                $response['reg_no'] = $model->reg_no;
                $response['relation'] = $model->relationship;
                $response['address'] = $model->address;
                return  json_encode($response); 
            }else {

                return true;
            }
        }
		
		/*echo "<pre>";
		echo print_r($model);
		echo "</pre>";*/

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Patient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        $model->updated_by = Yii::$app->user->id;
        $model->updated_on = date("Y-m-d H:i:s");

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return true;
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);


    }

    /**
     * Deletes an existing Patient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model  = $this->findModel($id);
        $model->status = '0';
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Patient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Patient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Patient::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionValidate()
    {

        if($_GET['id'])
        {
            $model = $this->findModel($_GET['id']);
            $model->scenario = 'update';
        }
        else
        {
            $model = new Patient();
            $model->scenario = 'create';
        }

        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
}
