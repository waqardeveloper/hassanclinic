<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "checkin".
 *
 * @property int $id
 * @property int $patient_id
 * @property int $invoice_no
 * @property string $status 0 for active 1 for inactive
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class CheckIn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'checkin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'patient_id'], 'required'],
            [[ 'patient_id', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient',
            'invoice_no' => 'Invoice No',
            'status' => 'Status',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);

    }
}
