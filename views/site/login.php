<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->


    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
       // 'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}{input}{error}",
            'options' => ['class' => 'form-group mb-3'],
            'labelOptions' => ['class' => 'control-label'],
        ],
        'errorCssClass' => 'has-danger'
    ]); ?>

    <?php
    // Input group
    echo $form->field($model, 'username', [
    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-user"></i>
										</span>
									</span></div>',
    ])->textInput(['class' => 'form-control form-control-lg']);

    ?>

    <?php
    // Input group
    echo $form->field($model, 'password', [
        'inputTemplate' => '<div class="input-group">{input}<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
									</span></div>',
    ])->passwordInput(['class' => 'form-control form-control-lg']);

    ?>


    <div class="row">
        <div class="col-sm-8">
            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"checkbox-custom checkbox-default\">{input} {label}</div>",
            ]) ?>
        </div>
        <div class="col-sm-4 text-right">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>




        <!--<?/* $form->field($model, 'password')->passwordInput()*/?> !-->



    <?php ActiveForm::end(); ?>

</div>
