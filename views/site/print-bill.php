<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Bill</title>
    <link rel="stylesheet" href="<?= Yii::$app->homeUrl?>css/normalize.css">

    <link rel="stylesheet" href="<?= Yii::$app->homeUrl?>css/paper.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/notonastaliqurdudraft.css">
    <style>@page { size: custom }</style>
</head>

<body class="custom">

<section class="sheet padding-10mm">
    <div class="container">
        <div class="content-left">
            <h2>Hassan Lab</h2>
            <p>Chao Khalsa Disst Rawalpindi</p>
            <p>051357118</p>
        </div>
        <div class="content-center">
            <img src="<?= Yii::$app->homeUrl?>images/moon.svg" alt="Logo" height="64" width="64">
        </div>
        <div class="content-right">
            <h2>حسن لیب</h2>
            <p>چو آخا لصہ ضلع راولپنڈی</p>
            <p>051357118</p>
        </div>
    </div>
    <hr>
    <table style="width:108%;border: none;">
        <tr>
            <td>Bill No:</td>
            <td><input type="text" name="regno" value="<?= $model->invoice_no?>"></td>
            <td>Name:</td>
            <td><input type="text" name="username" value="<?= $model->patient->name?>/<?= $model->patient->reg_no?>"></td>
        </tr>
        <tr>
            <td>Age:</td>
            <td><input type="text" name="age" value="<?= $model->patient->age?> Y"></td>
            <td>Phone No:</td>
            <td><input type="text" name="phone" value="<?= $model->patient->phone_no?>"></td>
        </tr>
        <tr>
            <td>Gender:</td>
            <td><input type="text" name="gender" value="<?= $model->patient->gender?>"></td>
            <td>Relationship:</td>
            <?php
            if(empty($model->patient->relationship))
            {
                $relation = '';
            }else
            {
                $relation =$model->patient->relationship . ' ('.$model->patient->relationship_of.')';
            }?>

            <td><input type="text" name="relationship" value="<?=  $relation ?>"></td>
        </tr>
    </table>
    <hr>
</section>

</body>

</html>
