<?php

/* @var $this yii\web\View */

use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\User;

$this->title = 'Check IN';
?>
<style>

</style>
<div class="site-index">

    <div class="row">
        <div class="col-md-8">
            <div class="card-body">
            <div class="feature-list-index">

                <?php Pjax::begin(['timeout' => '30000']);
                ?>

                <?php  echo $this->render('_searchPatient', ['model' => $searchModel]); ?>

                <br>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout'=>"{pager}\n{items}\n{summary}",
                    'summary' => "Showing {begin} - {end} of {totalCount} items",
                    'tableOptions' => [
                        'class' => 'table table-condensed table-sm',
                    ],

                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        /*// 'id',
                         'name',
                         'type',
                         'value',
                         'required',
                         //'created_on',
                         //'created_by',
                         //'updated_on',
                         //'updated_by',*/

                        // 'id',
                        'reg_no',
                        'name',
                        //'cnic',
                        'phone_no',
                        'age',
                        //'email',
                        'gender',
                        [
                            'attribute'=>'relationship',
                            'value'=>function($model)
                            {
                                return $model->relationship . ' ('.$model->relationship_of.') ';
                            }

                        ],
                        'address',
                        //'whatsapp_no',
                        //'city',
                        //'country',

                        //'referred_by_id',
                        //'panel_id',
                        //'status',

                        //'updated_on',
                        //'updated_by',
                        [
                            'class' => '\kartik\grid\ActionColumn',
                            'header'=>'Actions',
                            'template'=>'{attach}',
                            'buttons' => [
                                'width' => '100px',
                                'attach' => function ($url, $model)

                                {
                                        return '<a href="#"><span class="fas fa-arrow-right"  onclick="attachPatient('.$model->id.',event)"></span></a>';

                                } ,

                            ]

                        ]

                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
                </div>


        </div>
        <div class="col-md-4">
            <div class="card-body">
            <div id="receiptData">
                <div id="receipt-data">
                    <div class="text-center">
                        <p style="
                                font-size: 24px;
                                color: #0088cc;
                                font-weight: bolder;
                                padding-top: 4px;
                                ">
                            Hassan Lab

                        </p>
                            <table class="table table-responsive-lg table-bordered table-striped table-sm mb-0">

                                <tbody>
                                <tr>
                                    <td>Slip No</td>
                                    <td><?= $model->invoice_no?></td>

                                </tr>

                                <tr>
                                    <td>Registration No</td>
                                    <td><span id="reg_no"></span></td>

                                </tr>

                                <tr>
                                    <td>Name</td>
                                    <td><span id="name"></span></td>

                                </tr>

                                <tr>
                                    <td>Contact No</td>
                                    <td><span id="phone"></span>  </td>

                                </tr>

                                <tr>
                                    <td>Age</td>
                                    <td><span id="age"></span></td>

                                </tr>

                                <tr>
                                    <td>Gender</td>
                                    <td><span id="gender"></span>  </td>

                                </tr>

                                <tr>
                                    <td>Relation Ship</td>
                                    <td><span id="relation"></span>  </td>

                                </tr>


                                </tbody>
                            </table>

                        <br>


                        <p class="text-center"> Thank you for visiting</p>

                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'check-in',
                            //'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
                            'options' => ['id' => 'dynamic-form111'],
                            'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'check-in/validate':Yii::$app->homeUrl.'check-in/validate?id='.$model->id.'',
                            'options' => [
                                'class' => 'form-horizontal form-bordered'
                            ],
                            'errorCssClass' => 'has-danger',
                        ]);
                        ?>

                        <?php echo $form->field($model, 'patient_id')->hiddenInput()->label(false); ?>

                        <?= Html::submitButton('Submit', ['class' => 'mb-1 mt-1 mr-1 btn btn-primary btn-sm']) ?>

                        <a href="<?= Yii::$app->homeUrl?>site/index" class="mb-1 mt-1 mr-1 btn btn-danger btn-sm">Cancel</a>

                        <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div>
            </div>
    </div>
</div>
    <script>
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+"/web/";

        function attachPatient(id,event) {

           

            event.preventDefault();

            $.ajax({
                type: "GET",
                url: baseUrl+'ajax/get-patient?id='+id,
                // serializes the form's elements.
                success: function(data)
                {
                    if(data)
                    {
                        $('#checkin-patient_id').val(id);
                        var obj = JSON.parse(data);
                        $('#reg_no').html(obj.reg_no);
                        $('#name').html(obj.name);
                        $('#age').html(obj.age);
                        $('#phone').html(obj.phone_no);
                        $('#gender').html(obj.gender);
                        $('#relation').html(obj.relation);
                        console.log(obj);
                        bootbox.hideAll();
                    }


                }
            });
           
        }

        function add_patient(event,obj) {

            event.preventDefault();
            var url = '<?= Yii::$app->homeUrl ?>'+'patient/create?check-in=1';
            console.log(obj.text);
            var dialog = bootbox.dialog({
                title: "Add New Patient",
                message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

            });

            dialog.init(function(){
                var request = $.ajax({
                    url: url,
                    method: "GET",
                });

                request.done(function( msg ) {
                    dialog.find('.bootbox-body').html(msg);
                });

                $(document).on("submit", "#form", function (event) {
                    event.preventDefault();
                    event.stopImmediatePropagation();


                    $form = $(this); //wrap this in jQuery

                    var url = $form.attr('action');

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $("#form").serialize(),
                        // serializes the form's elements.
                        success: function(data)
                        {
                            if(data)
                            {

                                var obj = JSON.parse(data);
                                $('#checkin-patient_id').val(obj.id);
                                $('#reg_no').html(obj.reg_no);
                                $('#name').html(obj.name);
                                $('#age').html(obj.age);
                                $('#phone').html(obj.phone_no);
                                $('#gender').html(obj.gender);
                                $('#relation').html(obj.relation);
                                console.log(obj);
                                bootbox.hideAll();
                            }


                        }
                    });
                    // avoid to execute the actual submit of the form.

                });

            });

        };


        $(document).on('pjax:success', function() {
            window.history.pushState('page2', 'Title', baseUrl+'site/index');
        });


        $(document).ready(function () {
            $('body').on('beforeSubmit', 'form#check-in', function () {
                var form = $(this);
                // return false if form still have some validation errors
                if (form.find('.has-error').length)
                {
                    return false;
                }
                // submit form
                $.ajax({
                    url    : form.attr('action'),
                    type   : 'post',
                    data   : form.serialize(),
                    success: function (response)
                    {
                        console.log(response);
                        var width = 900;
                        var height = 700;
                        var url  = '<?=Yii::$app->homeUrl?>site/print-bill?id='+response;
                        console.log(url);
                        var printWindow = window.open(url, 'Print', 'left=100, top=100, width=' + width + ', height=' + height + ', toolbar=0, resizable=0');
                        printWindow.addEventListener('load', function(){
                            printWindow.print();
                            printWindow.close();
                        }, true);
                        location.reload();
                    },
                    error  : function ()
                    {
                        console.log('internal server error');
                    }
                });
                return false;
            });
        });


    </script>