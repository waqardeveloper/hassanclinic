<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$baseUrl = Yii::$app->homeUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="<?= Yii::$app->homeUrl?>src/assets/global/plugins/jquery.min.js"></script>
</head>
<body class="page-container-bg-solid ">
<?php $this->beginBody() ?>
<section class="body">

    <!-- start: header -->
    <header class="header header-nav-menu header-nav-top-line">
        <div class="logo-container">
            <a href="#" class="logo" style="
                                font-size: 24px;
                                color: #0088cc;
                                font-weight: bolder;
                                padding-top: 4px;
                                ">
                Hassan Lab

            </a>
            <button class="btn header-btn-collapse-nav d-lg-none" data-toggle="collapse" data-target=".header-nav">
                <i class="fas fa-bars"></i>
            </button>

            <!-- start: header nav menu -->
            <div class="header-nav collapse">
                <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 header-nav-main-square">
                    <nav>
                        <ul class="nav nav-pills" id="mainNav">
                            <?php
                            
                            if(\Yii::$app->user->can('check-in/create'))
                            {
                            
                            ?>
                            <li class="">
                                <a class="nav-link" href="<?= $baseUrl?>site/index">
                                    Front Desk
                                </a>
                            </li>
                           <?php } ?>

                            <?php

                            if(\Yii::$app->user->can('check-in/index'))
                            {

                            ?>

                            <li class="">
                                <a class="nav-link uc" href="<?= $baseUrl?>check-in">
                                    Check In List
                                </a>
                            </li>
                            <?php } ?>

                            <?php

                            if(\Yii::$app->user->can('patient/index'))
                            {

                            ?>
                            <li class="">
                                <a class="nav-link uc" href="<?= $baseUrl?>patient">
                                    Patients List
                                </a>
                            </li>
                            <?php } ?>
                            <?php

                            if(\Yii::$app->user->can('reports/index'))
                            {

                            ?>
                            <li class="dropdown">
                                <a class="nav-link dropdown-toggle" href="#">
                                    Reports
                                    <i class="fas fa-caret-down"></i></a>
                                <ul class="dropdown-menu">


                                    <li>
                                        <a class="nav-link" href="#">
                                            Today Visit Report
                                        </a>
                                    </li>
                                    <li>
                                        <a class="nav-link" href="#">
                                            Custom Visit Report
                                        </a>
                                    </li>
                                    <li>
                                        <a class="nav-link" href="#">
                                            Patient History Report
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <?php } ?>


                            <?php
                                if(\Yii::$app->user->can('user/index'))
                                {
                            ?>

                            <li class="dropdown">
                                <a class="nav-link dropdown-toggle" href="#">
                                   More
                                    <i class="fas fa-caret-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a class="nav-link" href="<?= $baseUrl?>user">
                                            Users
                                        </a>
                                    </li>
                                    <li>
                                        <a class="nav-link" href="<?= $baseUrl?>rbac">
                                            Role Based Access Control
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <?php } ?>

                        </ul>
                    </nav>
                </div>
            </div>
            <!-- end: header nav menu -->
        </div>

        <!-- start: search & user box -->
        <div class="header-right">

            <a class="btn search-toggle d-none d-md-inline-block d-xl-none" data-toggle-class="active" data-target=".search"><i class="fas fa-search"></i></a>
            <form action="pages-search-results.html" class="search nav-form d-none d-xl-inline-block">
                <div class="input-group">
                    <input type="text" class="form-control" name="q" id="q" placeholder="Search Patient Record...">
							<span class="input-group-append">
								<button class="btn btn-default" type="submit"><i class="fas fa-search"></i></button>
							</span>
                </div>
            </form>

            <span class="separator"></span>


            <div id="userbox" class="userbox">
                <a href="#" data-toggle="dropdown">
                    <figure class="profile-picture">
                        <img src="<?=\app\helpers\Helper::getBaseUrl().'files/profile_image/'?><?=Yii::$app->user->identity->image?>" alt="<?=Yii::$app->user->identity->username?>" class="rounded-circle" data-lock-picture="<?=$baseUrl?>porto/img/!logged-user.jpg" />
                    </figure>
                    <div class="profile-info" data-lock-name="<?=Yii::$app->user->identity->username?>" data-lock-email="<?=Yii::$app->user->identity->email?>">
                        <span class="name"><?=Yii::$app->user->identity->username?></span>
                        <span class="role">

                            <?php
                            $i=0;
                            $role = \Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id);
                            foreach ($role as $key=>$r)
                            {

                                if($i>0)
                                {
                                    echo $key.",";
                                }
                                $i++;
                            }


                            ?>




                        </span>
                    </div>

                    <i class="fa custom-caret"></i>
                </a>

                <div class="dropdown-menu">
                    <ul class="list-unstyled">
                        <li class="divider"></li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="<?= $baseUrl?>user/profile"><i class="fas fa-user"></i> My Profile</a>
                        </li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="<?=$baseUrl?>site/logout"><i class="fas fa-power-off"></i> Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end: search & user box -->
    </header>
    <!-- end: header -->
    <div class="inner-wrapper">
        <section role="main" class="content-body">
            <header class="page-header">
                <h2><?= Html::encode($this->title) ?></h2>

                <!--<div class="right-wrapper text-right">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="fas fa-home"></i>
                            </a>
                        </li>
                        <li><span>Layouts</span></li>
                        <li><span>Default</span></li>
                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>-->
            </header>
            <?= $content?>
        </section>

    </div>


</section>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
