<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CheckIn */
/* @var $form yii\widgets\ActiveForm */

$formatJs = <<< 'JS'
var formatRepo = function (repo) {
console.log(repo);
    if (repo.loading) {
        return repo.text;
    }
    console.log(repo);
    
     var markup =
    '<div id="userbox" class="userbox">'+
						'<a href="#" data-toggle="dropdown">'+
							'<figure class="profile-picture">'+
								'<img src="https://static.thenounproject.com/png/27705-200.png" alt="Joseph Doe" class="rounded-circle" data-lock-picture="https://static.thenounproject.com/png/27705-200.png">'+
							'</figure>'+
							'<div class="profile-info" data-lock-name="' + repo.name + '" >'+
								'<span class="name">' + repo.name + '</span>'+
								'<span class="role">' + repo.reg_no + '/'+repo.phone_no+'</span>'+
							'</div>'+
						'</a>'+
					'</div>';
   
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
                        
                            $('#name').html(repo.name);
                            
                            $('#reg_no').html(repo.reg_no);
                            $('#name').html(repo.name);
                            $('#age').html(repo.age);
                            $('#phone').html(repo.phone_no);
                            $('#gender').html(repo.gender);
                            $('#relation').html(repo.relation);
                            
                            
                           
    return repo.name;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data, params) {
console.log(data);
    params.page = params.page || 1;
    return {
        results: data,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;



?>

<style>
    
    .control-label{
        font-size: 0.9rem !important;
        color: black;
    }
    * { margin: 0; padding: 0; }
    body { font: 14px/1.4 Georgia, serif; }
    #wrap { width: 320px; margin: 0 auto; }
    .col-center{float: left; width: 100%; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt}
    .pad-left-right{padding-left: 0px; padding-right: 0px;}
    .text-center{ text-align:center;}
    .col-email{float: left; width: 100%; font-family:"Times New Roman", Georgia, Serif; font-size:9pt; margin-top: 10px;}
    .item-n{width: 100%; float: left; text-align: right}
    .item-num{width: 100%; float: left; border-bottom: 1px dashed #000;}
    .item-qty{width: 100%; float: left;}






    textarea { border: 0;
        font: 12px Georgia, Serif;
        overflow: hidden;
        resize: none;
        text-align: center; }
    table { border-collapse: collapse; }
    table td, table th { border: 1px solid #e3d9d9; padding: 5px; }

    .d-area{width: 100%;  padding:2px; float: left; margin-bottom: 5px; }
    .time{width: 48%; padding:5px 0; float:left;font-size: 9pt;  }
    .date{width: 48%;
        padding: 5px 0;
        float: left;
        text-align: right;
        border-left: 1px solid #cecece;
        font-size: 9pt;
    }
    .date_chng{
        font-family: sans-serif;
        font-size: 12px;
    }
    .fnt_weight{
        font-weight: 800;
    }

    #cus-in { margin-top: 6px; width: 100%; float: right; font:13px Helvetica, Sans-Serif; }
    #cus-in span {font: bold 15px Helvetica, Sans-Serif; }
    #cus-in  td { text-align: right; width: 65%; border: 1px solid #e6e1e1 }
    #cus-in  td.cus-in-head { text-align: left;}
    #cus-in  td textarea { width: 100%; height: 20px; text-align: right; }


    #address { width: 250px; height: 150px; float: left; }
    #customer { overflow: hidden; }



    #items { clear: both; width: 100%; text-align: center; margin: 10px 0 0 0; float: left; border: 1px solid black; font: bold 13px Helvetica, Sans-Serif; }
    #items th { background: #eee; }
    #items textarea { width: 80px; height: 26px; font: 12px Helvetica, Sans-Serif; }
    #items tr.item-row td { border: 0; vertical-align: top; border: 1px solid #f3f3f3; font: 13px Helvetica, Sans-Serif; }
    #items td.description { width: 40%; }
    #items td.item-name { width: 37%; }
    #items td.description textarea, #items td.item-name textarea { width: 100%; }
    #items td.total-line { border-right: 0; text-align: right; width: 65%; }
    #items td.total-value { border-left: 0; padding: 5px;text-align: right;}
    #items td.total-value textarea { height: 20px; background: none; }

    #items td.blank { border: 0; }

    #comment { text-align: center; margin: 20px 0 0 0;  float: left;width: 100%;}
    #comment h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
    #comment textarea { width: 100%; text-align: center;}

    .delete-wpr { position: relative; }
    .delete { display: block; color: #000; text-decoration: none; position: absolute; background: #EEEEEE; font-weight: bold; padding: 0px 3px; border: 1px solid; top: -6px; left: -22px; font-family: Verdana; font-size: 12px; }
    .breakpg{
        page-break-after  : always;
        page-break-before : always;
        page-break-inside : auto    ;
    }
    #page-wrap {
        width: 320px;
        float: left;
    }

    @media print{
        #wrap { width: 248px; margin: -30px auto; }
        #page-wrap {    width: 248px;    margin: 0 auto; float:left}
        .breadcrumb{display: none}
        p{none}
        .text-left{font-size: 12px; text-align: center; float: left}
        .wrap > .container { padding: 5px 0px 0px;}

    }
</style>

<section class="card">
    <header class="card-header">

        <h2 class="card-title">Invoice No : <?= $model->invoice_no?></h2>

    </header>
    <div class="card-body">

        <div class="check-in-form">

            <?php
            $form = ActiveForm::begin([
                'id' => 'check-in',
                //'enableClientValidation' => true,
               'enableAjaxValidation' => true,
                'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'check-in/validate':Yii::$app->homeUrl.'check-in/validate?id='.$model->id.'',
                'options' => [
                    'class' => 'form-horizontal form-bordered'
                ],
                'errorCssClass' => 'has-danger',
            ]);
            ?>

            <?php // $form->field($model, 'id')->textInput() ?>
            <div class="row">
                <!-- <div class="col-md-4"></div> -->
                <div class="col-md-4 offset-md-3" style="margin-top: 10px;">

                    <?php
                    echo $form->field($model, 'patient_id')->label(false)->widget(Select2::classname(), [
                        //'data' => $data,
                        'options' => ['placeholder' => 'Search for a Patient ...','class'=>'bhutta'],
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [

                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'ajax' => [
                                'url' => Yii::$app->homeUrl.'ajax/search-patient',
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ],
                    ]);


                    ?>

                </div>
                <div class="col-md-4">
                    <button onclick="add_patient(event,this)" class="mb-1 mt-1 mr-1 btn btn-success">Add New Patient</button>
                </div>
            </div>


            <?php // $form->field($model, 'patient_id')->textInput() ?>

            <?php // $form->field($model, 'invoice_no')->textInput() ?>

            <?php // $form->field($model, 'status')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

            <?php // $form->field($model, 'created_on')->textInput() ?>

            <?php // $form->field($model, 'created_by')->textInput() ?>

            <?php //$form->field($model, 'updated_on')->textInput() ?>

            <?php // $form->field($model, 'updated_by')->textInput() ?>

            <div class="form-group">
                <?php // Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>


            <div id="wrap">

                <div class="col-center  pad-left-right text-center">
                    <img src="<?= Yii::$app->homeUrl?>porto/img/logo.png" width="210">
                    <!--        <p style="text-align: left; margin-top: 5px;"><b>FOR INQUIRIES SALE:  051-2324256</b></p>-->
                    <!--        <p style="text-align: left; margin-top: 5px;"><b>FOR CUSTOMER SERVISES: 051-2323567</b></p>-->

                </div>
                <br>

                <div id="page-wrap">

                    <!--time date Section-->
                    <div class="d-area">
                        <div class="time">Invoice No: <?= $model->invoice_no?></div>
                        <div class="date">Date: <?= date("d/m/Y h:i A", strtotime($model->created_on));?></div>
                    </div>
                    <!--time date Section-->

                    <table id="d-area" class="date_chng" style="width: 100%">
                        <tbody><tr>

                            <td>
                                <span class="fnt_weight">Reg No#</span> <br>
                                <span id="reg_no"></span>            </td>

                            <td class="cus-in-head">
                                <span class="fnt_weight">Name</span> <br>
                                <span id="name"></span>            </td>
                        </tr>

                        <tr>

                            <td>
                                <span class="fnt_weight">Contact No</span> <br>
                                <span id="phone"></span>                           </td>

                            <td class="cus-in-head">
                                <span class="fnt_weight">Age</span> <br>
                                <span id="age"></span>            </td>
                        </tr>


                        <tr>
                            <td class="cus-in-head">
                                <span class="fnt_weight">Gender</span> <br>
                                <span id="gender"></span>           </td>

                            <td>
                                <span class="fnt_weight">Relation Ship</span> <br>
                                <span id="relation"></span>            </td>
                        </tr>

                        <!--<tr>
                            <td>
                                <span class="fnt_weight">User</span><br>
                                            </td>

                            <td>
                                <span class="fnt_weight">User</span><br>
                                            </td>
                        </tr>-->
                        </tbody></table>

                    <!--time date Section-->
                    <div class="d-area">
                        <div style="font-size: 12px;" class="text-center"><b>Customer's Copy</b></div>
                    </div>
                    <div class="d-area">
                        <div style="font-size: 8px;" class="text-center">The slip has been generated by <b>admin</b></div>
                    </div>
                    <!--time date Section-->










                </div>
            </div>





        </div>
    </div>
    <footer class="card-header text-center">



        <a href="<?=Yii::$app->homeUrl?>check-in/create" class="mb-1 mt-1 mr-1 btn btn-danger">Cancel</a>
        <?= Html::submitButton('Save & New', ['class' => 'mb-1 mt-1 mr-1 btn btn-primary']) ?>
        <button type="button" class="mb-1 mt-1 mr-1 btn btn-primary">Save</button>


    </footer>
    <?php ActiveForm::end(); ?>
</section>


<script>
    function add_patient(event,obj) {

        event.preventDefault();
        var url = '<?= Yii::$app->homeUrl ?>'+'patient/create?check-in=1';
        console.log(obj.text);
        var dialog = bootbox.dialog({
            title: "Add New Patient",
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();


                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data)
                        {
                            var obj = JSON.parse(data);
                            $("#checkin-patient_id").select2("trigger", "select", {
                                data: { id: obj.id, text: obj.name }
                            });
                            $('.select2-selection__clear').after(obj.name);
                            //$("#checkin-patient_id").select2("data", { id: 1, text: "Some Text" });
                            //$('#checkin-patient_id').select2().val(obj.name).trigger('change');
                            $('#reg_no').html(obj.reg_no);
                            $('#name').html(obj.name);
                            $('#age').html(obj.age);
                            $('#phone').html(obj.phone);
                            $('#gender').html(obj.gender);
                            $('#relation').html(obj.relation);
                            console.log(obj);
                            bootbox.hideAll();
                        }


                    }
                });
                // avoid to execute the actual submit of the form.

            });

        });

    };

    $( document ).ready(function() {
        $('.select2-selection__placeholder').html("Search For Patient.......")
    });

</script>

