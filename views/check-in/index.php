<?php

use app\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CheckInSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Check In';
$this->params['breadcrumbs'][] = $this->title;

function statusLabel($status)
{
    if ($status !='1')
    {
        $label = "<span class=\"highlight-danger\">".Yii::t('app', 'Inactive')."</span>";
    }
    else
    {
        $label = "<span class=\"highlight\">".Yii::t('app', 'Active')."</span>";
    }
    return $label;
}
$status = array('0'=>Yii::t('app', 'Inactive'),'1'=>Yii::t('app', 'Active'));
?>
<div class="check-in-index">


<!--    <h1><?/*= Html::encode($this->title) */?></h1>
-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            //'{export}',
            '{toggleData}',
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'exportConfig' => [
            GridView::CSV => ['label' => 'Save as CSV', 'icon' => 'file-excel'],
            GridView::EXCEL => ['label' => 'Save as EXCEL'],
            GridView::TEXT => ['label' => 'Save as TEXT'],
            GridView::PDF => ['label' => 'Save as PDF'],

        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before'=>' '.Html::a('<i class="fa fa-sync"></i> '.Yii::t ( 'app', 'Reset List' ), ['index'], ['class' => 'btn btn-info btn-sm']).' ',

            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'created_on',
                'label'=>'Date',
                'value' => function($model){
                    return date("d/m/Y h:i A", strtotime($model->created_on));
                },
                'filterType'=>GridView::FILTER_DATE,
                'filterWidgetOptions'=> [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy',
                    ],
                ],
                //'filter'=>false

            ],
            'invoice_no',
            [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'patient_id',
                'label'=>'Patient Name',
                'value' => 'patient.name',
                //'filter'=>false

            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'patient_id',
                'label'=>'Reg ID #',
                'value' => 'patient.reg_no',
                //'filter'=>false

            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'patient_id',
                'label'=>'Phone Number',
                'value' => 'patient.phone_no',
                //'filter'=>false

            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'patient_id',
                'label'=>'Age',
                'value' => 'patient.age',
                //'filter'=>false

            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'patient_id',
                'label'=>'Gender',
                'value' => 'patient.gender',
                //'filter'=>false

            ],

            [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'patient_id',
                'label'=>'Relation Ship',
                'value'=>function($model)
                {
                    if($model->patient->relationship)
                    return $model->patient->relationship . ' ('.$model->patient->relationship_of.') ';
                    else
                        return '';
                }
                //'filter'=>false

            ],
            [
                'attribute'=>'status',
                'width' => '125px',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'visible'=>\Yii::$app->user->can('deleteView'),
                'value'=>function($model)
                {
                    return statusLabel($model->status);
                },
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => [
                    0 => 'Inactive',
                    1 => 'Active',
                ],
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',
            ],
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'attribute' => 'created_by',
                'contentOptions' => ['class' => 'text-center'],
                'value'=>'user.username',
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'width' => '80px',
                'header'=>'Files',
                //'enableRowClick'=>true,
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function ($model, $key, $index, $column) {
                    return Yii::$app->controller->renderPartial('file', ['model' => $model]);
                },
                // 'expandOneOnly' => true,
                'mergeHeader'=>false,
            ],
            /*[
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'attribute' => 'created_by',
                'label'=>'Files',
                'width' => '200px',
                'contentOptions' => ['class' => 'text-center'],
                'value' => function ($model, $key, $index, $widget)
                {
                        $users = '<div class="project-people"><div class="popup-gallery">
';
                        $files = \app\models\Files::find()->where(['checkin_id'=>$model->id])->all();

                        foreach ($files as $file) {
                            //$path =  Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . $model->patient->reg_no . DIRECTORY_SEPARATOR.$file->file_name;
                            $home = Yii::$app->homeUrl;
                            $home = str_replace("/web","/files",$home);
                            $path = $home. $model->patient->reg_no.'/'.$file->file_name;

                            if((strpos($file->type, 'image') !== false)){
                                $image = '<img  class="img-circle" style="width: 35px;height: 40px;"  src="'.$path.'">';

                            }else {
                                $image = '<img  class="img-circle" style="width: 35px;height: 40px;"  src="'.Yii::$app->homeUrl.'images/file.png'.'">';


                            }

                            $first_name = $file->file_name;
                            $last_name = $file->custom_name;
                            $username = $file->type;

                            //$users .= $image;

                            $users .= ' <a  href="'.$path.'"   data-toggle="hover" data-placement="top" data-content="' . $first_name . ' ' . $last_name . ' (' .$username. ')">' . $image . '</a>';
                        }
                        $users .= '</div></div>';
                        return $users;

                },

                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],*/

            //'updated_on',
            //'updated_by',
            [

                //'attribute' =>  'created_on',
                'header'=>'Last Update',
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'format'=>'raw',
                'visible'=> \Yii::$app->user->can('check-in/last-update'),
                'value' => function($model){
                    if($model->updated_by)
                    {
                        return date("d/m/Y h:i A", strtotime($model->updated_on)).'<br>'.\app\helpers\Helper::getUser($model->updated_by);

                    }else
                    {
                        return '';
                    }
                },
                //'filter'=>false

            ],

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{attach}  {bill}   {delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        if (\Yii::$app->user->can('check-in/delete')) {
                            return Html::a('<span class="fas fa-trash"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-pjax' => '1',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                        'pjax' => 1,],
                                ]
                            );
                        }
                    },
                    'bill' => function ($url, $model)

                    {
                        if(\Yii::$app->user->can('site/print-bill')){
                            return '<a href="'.Yii::$app->homeUrl.'site/print-bill?id='.$model->id.'" target="_blank"><span class="fa fa-file"></span></a>';

                        }else
                        {
                            return '';
                        }
                    } ,
                    'update' => function ($url, $model)

                    {
                        if(\Yii::$app->user->can('check-in/update')){
                            return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'patient\',\'Update Patient\',event)"></span></a>';

                        }else
                        {
                            return '';
                        }
                    } ,
                    'attach' => function ($url, $model)

                    {
                        if(\Yii::$app->user->can('check-in/attach-file')){
                            return '<a href="'.Yii::$app->homeUrl.'check-in/attach-file?id='.$model->id.'"><span class="fas fa-paperclip mr-2 "></span></a>';

                        }else
                        {
                            return '';
                        }
                    } ,


                ],
            ],
            //'created_by',
            //'updated_on',
            //'updated_by',

        ],
    ]); ?>

</div>
