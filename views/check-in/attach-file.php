<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 9/2/2018
 * Time: 9:49 AM
 */

 use dosamigos\fileupload\FileUploadUI;
use yii\widgets\DetailView;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\CheckIn */

$this->title = 'Attach Files';

?>
    <section class="card">
    <header class="card-header">

        <h2 class="card-title">Attach Files</h2>

    </header>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td><strong><span class="text-danger">Invoice Number</span></strong></td>
                    <td><?= $model->invoice_no?></td>
                    <td><strong><span class="text-danger">Added On</span></strong></td>
                    <td>
                        <?= date("d/m/Y H:i A", strtotime($model->created_on));?>


                    </td>
                </tr>
                <tr>
                    <td><strong><span class="text-danger">Patinent Name</span></strong></td>
                    <td><?= $model->patient->name?></td>
                    <td><strong><span class="text-danger">Reg No</span></strong></td>
                    <td><?= $model->patient->reg_no?></td>
                </tr>
                <tr>
                    <td><strong><span class="text-danger">Contact Number</span></strong></td>
                    <td><?= $model->patient->phone_no?></td>
                    <td><strong><span class="text-danger">Age</span></strong></td>
                    <td><?= $model->patient->age?></td>

                </tr>
                <tr>
                    <td><strong><span class="text-danger">Gender</span></strong></td>
                    <td><?= $model->patient->gender?></td>
                    <td><strong><span class="text-danger">RelationShip</span></strong></td>
                    <td><?= $model->patient->relationship?></td>
                </tr>

                <tr>
                    <td><strong><span class="text-danger">Address</span></strong></td>
                    <td><?= $model->patient->address?></td>
                    <td><strong><span class="text-danger">Added By</span></strong></td>
                    <td><?= $model->user->username?></td>
                </tr>

                </tbody>
            </table>





        </div>


        <?= FileUploadUI::widget([
            //'model' => $model,
            'name' => 'Files[file]',
            'url' => ['site/file-upload?check-in='.$model->id.''],
            'formView'=>'@app/views/files/form',
            //'uploadTemplateId'=>'template-upload-custom',
            'downloadTemplateView' => '@app/views/files/download',
            'uploadTemplateView' => '@app/views/files/upload',
            'gallery' => false,
            'fieldOptions' => [
                'accept' => 'image/*'
            ],
            'clientOptions' => [
                //'maxFileSize' => 2000000
            ],
            // ...
            'clientEvents' => [
                'fileuploadadd' => 'function(e, data) {
                                $(".start").removeClass("hidden");
                                $(".cancel").removeClass("hidden");
                                $(".thead").removeClass("hidden");
                                console.log("Waqar");
                                console.log(e);
                                console.log(data);
                            }',
                'fileuploaddrop' => 'function(e, data) {
                                console.log("Ahmed");
                              
                            }',
                'fileuploadsubmit' => 'function(e, data) {
                                var inputs = data.context.find(\':input\');
                                    if (inputs.filter(function () {
                                            return !this.value && $(this).prop(\'required\');
                                        }).first().focus().length) {
                                        data.context.find(\'button\').prop(\'disabled\', false);
                                        return false;
                                    }
                                    data.formData = inputs.serializeArray();

                                }',
            ],
        ]); ?>
<br>
        <br>


        <a class="btn btn-sm btn-default" href="<?= Yii::$app->homeUrl?>check-in">Go Back</a>
    </div>





</section>
