<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light',
        'porto/vendor/bootstrap/css/bootstrap.css',
        'porto/vendor/animate/animate.css',
        'porto/vendor/font-awesome/css/fontawesome-all.min.css',
        'porto/vendor/magnific-popup/magnific-popup.css',
        'porto/css/theme.css',
        'porto/css/skins/default.css',
        'css/custom.css',

    ];
    public $js = [

        //'porto/vendor/jquery/jquery.js',
        'porto/vendor/jquery-browser-mobile/jquery.browser.mobile.js',
        'porto/vendor/popper/umd/popper.min.js',
        'porto/vendor/bootstrap/js/bootstrap.js',
        'porto/vendor/common/common.js',
        'porto/vendor/nanoscroller/nanoscroller.js',
        



    ];
    public $depends = [



    ];
}
