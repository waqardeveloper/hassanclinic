<?php

namespace app\helpers;

use app\models\Settings;
use app\models\User;
use DateTime;
use Yii;
use yii\base\Exception;

/**
 * Created by PhpStorm.
 * User: Waqar
 * Date: 10/28/2017
 * Time: 2:35 PM
 */


class Helper
{



    public  static function getSize($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }



    public static function DateTime($dateTime)
    {
        $phpdate = strtotime( $dateTime );
        $date = date( 'd/m/Y', $phpdate );
        $time = date('h:i A',$phpdate);

        return $date.'<br>'.$time;
    }

    public static function getCreated($id)
    {
        $user = User::findOne($id);

        return  $user->username;
    }




    public static function getBaseUrl()
    {
        $url = Yii::$app->homeUrl;
        $str  = str_replace("/web","",$url);

        return $str;
    }


    public static function DatTim($dateTime)
    {
        $phpdate = strtotime($dateTime);
        $date = date('d/m/Y h:i A', $phpdate);


        return $date;
    }

    public static function getMySql($dateTime)
    {
        $phpdate = strtotime($dateTime);
        $date = date('d/m/Y h:i A', $phpdate);


        return $date;
    }

    public static function getUser($id)
    {
        $user = User::findOne($id);
        return $user->username;
    }

}
?>